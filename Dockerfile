FROM openjdk:8-jdk-alpine
WORKDIR /opt
EXPOSE 8030 
COPY /target/*.jar /opt/shop-ui.jar
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /opt/shop-ui.jar" ]




